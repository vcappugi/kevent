"""kevent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from kappm.views import home, participants, ParticipantView, SpeachView, speach, StandView, stand, ActivityView, activity,timeline, PersonalView, personal, ContingencyView, contingency, login, EventoView, evento, logoutnea, questionary, QuestionaryView, qrquestion, question, saveresponse, Winner

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'),
    #evento Back
    path('Evento/', EventoView.as_view(), name='Evento'),
    path('Evento/<int:id>/', EventoView.as_view(), name='Evento'),
    #participantesFront
    path('eventos/', evento, name='eventos'),


    #participantes Back
    path('Part/', ParticipantView.as_view(), name='Part'),
    path('Part/<int:id>/', ParticipantView.as_view(), name='Part'),
    #participantesFront
    path('participantes/', participants,name='participants'),

    #responsables
    path('Part/', ParticipantView.as_view(), name='Part'),
    path('Part/<int:id>/', ParticipantView.as_view(), name='Part'),
    #participantesFront
    path('participantes/', participants,name='participants'),

    #exposition Back
    path('Speach/', SpeachView.as_view(), name='Speach'),
    path('Speach/<int:id>/', SpeachView.as_view(), name='Speach'),
    #exposiciones Front
    path('speachs/', speach,name='speachs'),

    #Stands Back
    path('Stand/', StandView.as_view(), name='Stand'),
    path('Stand/<int:id>/', StandView.as_view(), name='Stand'),
    #exposiciones Front
    path('stands/', stand,name='stands'),

    #Activity Back
    path('Activity/', ActivityView.as_view(), name='Activity'),
    path('Activity/<int:id>/', ActivityView.as_view(), name='Activity'),
    #exposiciones Front
    path('activities/', activity, name='activities'),

    path('timeline/', timeline, name='timeline'),

    #personal Back
    path('Personal/', PersonalView.as_view(), name='Personal'),
    path('Personal/<int:id>/', PersonalView.as_view(), name='Personal'),
    #personal front
    path('personal/', personal, name='personal'),

     #contingency Back
    path('Contingency/', ContingencyView.as_view(), name='Contingency'),
    path('Contingency/<int:id>/', ContingencyView.as_view(), name='Contingency'),
    #contingency front
    path('contingency/', contingency, name='contingency'),

    #login
    path('login/', login, name='login'),
     path('logout/', logoutnea, name='logout'),

    #questionary
    path('Questionary/', QuestionaryView.as_view(), name='Questionary'),
    path('Questionary/<int:id>/', QuestionaryView.as_view(), name='Questionary'),
    path('Question/', questionary, name='Question'),

    path('qrquestion/<int:pk>',qrquestion, name='qrquestion' ),
    path('quest/<int:pk>',question, name='question' ),

    path('responsequestion/', saveresponse, name='responsequestion'),

    path('winners/<int:pk>', Winner, name='winners'),
    

    #path('part/', particip, name='part'),
    #path('regp/', reg_particip, name='regp')
]
