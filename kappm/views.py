
from ast import Try
from io import BytesIO
import io
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login as do_login, authenticate
from django.contrib.auth import logout as do_logout
from decouple import config
from django.views import View
import qrcode
import qrcode.image.svg 
from kappm.models import *
import json
import random

from decouple import config
# Create your views here.


servidor = "https://"+ config('SERVERHOST') +':'+ config('SERVERPORT')

def home(request):
    try:
        usuario =usuario_activo(request)
        template="home.html"
        idev = IDEvent.objects.get(id=1)
        servidor = config('SERVERHOST') +':'+ config('SERVERPORT')
        context={"idevent": idev, "serverhost": servidor}
        return render(request, template, context)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login





class EventoView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            objetos = list(IDEvent.objects.filter(id=id).values())
            if len(objetos)>0:
                objet = objetos[0]
                datos={'message':'Success...', 'Evento': objet}
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            objetos = list(IDEvent.objects.values())
            if len(objetos) > 0:
                datos = {'message':'Success', 'Evento': objetos}
                
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        IDEvent.objects.create(name=jd['name'],
        description=jd['description'],
        start_date = jd['start_date'],
        end_date = jd['end_date']
        )
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        objeto = list(IDEvent.objects.filter(id=id).values())
        if len(objeto) >0:
            eq=IDEvent.objects.get(id=id)
            eq.name = jd['name']
            eq.description = jd['description']
            eq.start_date=jd['start_date']
            eq.end_date=jd['end_date']
            
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Evento no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        stand = list(IDEvent.objects.filter(id=id).values())
        if len(stand) >0:
            IDEvent.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Evento no found'}
        return JsonResponse(datos)

def evento(request):
    try:
        usuario =usuario_activo(request)
        contexto = {"serverhost":servidor}
        template='evento.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login




    

class ParticipantView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            particip = list(Participants.objects.filter(id=id).values())
            if len(particip)>0:
                participant = particip[0]
                datos={'message':'Success...', 'Participante': participant}
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            particip = list(Participants.objects.values())
            if len(particip) > 0:
                datos = {'message':'Success', 'participante': particip}
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
       
        jd = json.loads(request.body)
        Participants.objects.create(idevent_id=jd['evento'],name=jd['name'],dni=jd['dni'],
        description_expositor=jd['description'], phone=jd['phone'],type_participant=jd['typep'],
        ubication=jd['ubication'], photo=jd['foto'])
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        responsable = list(Participants.objects.filter(id=id).values())
        if len(responsable) >0:
            eq=Participants.objects.get(id=id)
            eq.idevent_id=jd['evento']
            eq.name = jd['name']
            eq.dni=jd['dni']
            eq.description_expositor=jd['description']
            eq.representant=jd['representant']
            eq.ubication=jd['ubication']
            eq.phone=jd['phone']
            eq.type_participant=jd['typep']
            eq.photo=jd['foto']
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Responsable no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        participant = list(Participants.objects.filter(id=id).values())
        if len(participant) >0:
            Participants.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Participant no found'}
        return JsonResponse(datos)

def participants(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='participant.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login




class SpeachView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            speachs = list(Exposition.objects.filter(id=id).values())
            if len(speachs)>0:
                speach = speachs[0]
                datos={'message':'Success...', 'Speach': speach}
                x=0
                for l in speachs:
                    try:
                        spea = Exposition.objects.get(id=l['id'])
                        speachs[x]['paricipantname']=spea.participants.name
                    except :
                        speachs[x]['paricipantname']=''
                    x=x+1
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            speachs = list(Exposition.objects.values())
            if len(speachs) > 0:
                datos = {'message':'Success', 'Speach': speachs}
                x=0
                for l in speachs:
                    try:
                        spea = Exposition.objects.get(id=l['id'])
                        speachs[x]['paricipantname']=spea.participants.name
                    except:
                        speachs[x]['paricipantname']=''    
                    x=x+1
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        Exposition.objects.create(idevent_id=jd['idevent_id'],
        participants_id=jd['participants_id'],
        name=jd['name'],
        thema=jd['thema'],
        detail_exposition=jd['detail_exposition'], 
        estimate_time_min=jd['estimate_time_min'])
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        speachs = list(Exposition.objects.filter(id=id).values())
        if len(speachs) >0:
            eq=Exposition.objects.get(id=id)
            eq.idevent_id=jd['idevent_id']
            eq.name = jd['name']
            eq.participants_id=jd['participants_id']
            eq.thema=jd['thema']
            eq.detail_exposition=jd['detail_exposition']
            eq.estimate_time_min=jd['estimate_time_min']
           
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Speach no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        speach = list(Exposition.objects.filter(id=id).values())
        if len(speach) >0:
            Exposition.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Speach no found'}
        return JsonResponse(datos)


def speach(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='speach.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login




class StandView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            stands = list(Stands.objects.filter(id=id).values())
            if len(stands)>0:
                stand = stands[0]
                datos={'message':'Success...', 'Stands': stand}
                x=0
                for l in stands:
                    try:
                        sta = Stands.objects.get(id=l['id'])
                        stands[x]['paricipantname']=sta.participants.name
                    except:
                        stands[x]['paricipantname']=''
                    x=x+1
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            stands = list(Stands.objects.values())
            if len(stands) > 0:
                datos = {'message':'Success', 'Stands': stands}
                x=0
                for l in stands:
                    try:
                        sta = Stands.objects.get(id=l['id'])
                        stands[x]['paricipantname']=sta.participants.name
                    except:
                        stands[x]['paricipantname']=''
                    x=x+1
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        Stands.objects.create(idevent_id=jd['idevent_id'], cod=jd['cod'],
        participants_id=jd['participants_id'],
        name=jd['name'],
        tip=jd['tip'])

        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        speachs = list(Stands.objects.filter(id=id).values())
        if len(speachs) >0:
            eq=Stands.objects.get(id=id)
            eq.cod=jd['cod']
            eq.idevent_id=jd['idevent_id']
            eq.name = jd['name']
            eq.participants_id=jd['participants_id']
            eq.tip=jd['tip']
            
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Speach no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        stand = list(Stands.objects.filter(id=id).values())
        if len(stand) >0:
            Stands.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Stand no found'}
        return JsonResponse(datos)


def stand(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='stands.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login






class ActivityView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            activities = list(Activity.objects.filter(id=id).order_by(exact_date).values())
            if len(activities)>0:
                act = activities[0]
                datos={'message':'Success...', 'Activity': act}
                x=0
                for l in activities:
                    try:
                        act = Activity.objects.get(id=l['id'])
                        activities[x]['responsablename']=sta.personal.name
                    except:
                        activities[x]['responsablename']=''    
                    x=x+1
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            activities = list(Activity.objects.order_by('exact_date').values())
            if len(activities) > 0:
                datos = {'message':'Success', 'Activity': activities}
                x=0
                for l in activities:
                    try:
                        act = Activity.objects.get(id=l['id'])
                        activities[x]['responsablename']=act.personal.name
                    except:
                        activities[x]['responsablename']=''
                    x=x+1
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        Activity.objects.create(idevent_id=jd['idevent_id'],
        personal_id=jd['personal_id'],
        name=jd['name'],
        exact_date = jd['exact_date'],
        duration = jd['duration'],
        speach_id = jd['speach_id'],
        description = jd['description'],
        stand_id=jd['stand_id'])

        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        print("Este es el request body: ")
        print (jd)
        activities = list(Activity.objects.filter(id=id).values())
        if len(activities) >0:
            eq=Activity.objects.get(id=id)
            if (jd['estado'] == '0'):
                eq.idevent_id=jd['idevent_id']
                eq.name = jd['name']
                eq.description = jd['description']
                eq.exact_date = jd['exact_date']
                eq.personal_id=jd['personal_id']
                eq.stand_id = jd['stand_id']
                eq.speach_id=jd['speach_id']
                eq.status=False
            else:
                eq.status=True
            eq.save()
            #print('el responsable: ', jd['personal_id'])
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Activity no found'}
        return JsonResponse(datos)

    
            


    def delete(self, request, id):
        activities = list(Activity.objects.filter(id=id).values())
        if len(activities) >0:
            Activity.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Stand no found'}
        return JsonResponse(datos)


def activity(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='activity.html'
        return render(request, template, contexto)
    except Exception as ex:
        print( ex)
        return redirect('/login/')  # redircciona al url de  login


def timeline(request):
    activi = Activity.objects.all().order_by('exact_date')
    contin = Contingency.objects.all().order_by('id')
    quest = Questionary.objects.all()
    try:
        usuario =usuario_activo(request).username
    except:
        usuario = ''
    
    rt = []
    for r in activi:
        r1 = {}
        r1["name"] = r.name
        r1["description"]=r.description
        r1["id"] = r.id
        r1["status"] = r.status
        r1["duration"] = r.duration
        r1["responsablename"] = r.personal.name
        r1["speach"] = r.speach
        if (r.speach):
            r1['expositor']=r.speach.participants.name
            r1['foto'] = 'media/photos/'+ r.speach.participants.photo
        else:
            r1['expositor'] = ''
            r1['foto']=''
        r1['stand'] = r.stand
        r1['dateex']=r.exact_date
        r1['usuario'] = usuario
       
        try:
            quest = Questionary.objects.filter(activity_id=r.id)
            for q in quest:
                print (q.id)
                if q.id:
                    r1['question_id']=q.id
                    r1['question'] = q.question
                else:
                    r1['question_id']=''
                    r1['question']='' 
        except:
            r1['question_id']=''
            r1['question']='' 
        rt.append(r1)


    contexto ={'act':rt, "serverhost": servidor  }
    template ='timeline.html'
    return render(request, template, contexto)




class PersonalView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            personal = list(Personal.objects.filter(id=id).values())
            if len(personal)>0:
                per = personal[0]
                datos={'message':'Success...', 'Personal': per}
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            personal = list(Personal.objects.values())
            if len(personal) > 0:
                datos = {'message':'Success', 'Personal': personal}
                
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        Personal.objects.create(idevent_id=jd['idevent_id'],
        name=jd['name'],
        phone = jd['phone'],
        type_resp = jd['type_resp'])
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        persona = list(Personal.objects.filter(id=id).values())
        if len(persona) >0:
            eq=Personal.objects.get(id=id)
            eq.idevent_id=jd['idevent_id']
            eq.name = jd['name']
            eq.phone=jd['phone']
            eq.type_resp=jd['type_resp']
            
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Personal no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        stand = list(Personal.objects.filter(id=id).values())
        if len(stand) >0:
            Personal.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Personal no found'}
        return JsonResponse(datos)

def personal(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='personal.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login





class ContingencyView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            objetos = list(Contingency.objects.filter(id=id).values())
            if len(objetos)>0:
                objet = objetos[0]
                datos={'message':'Success...', 'Contingency': objet}
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            objetos = list(Contingency.objects.values())
            if len(objetos) > 0:
                datos = {'message':'Success', 'Contingency': objetos}
                
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)


    def post(self, request):
        jd = json.loads(request.body)
        Contingency.objects.create(idevent_id=jd['idevent_id'],
        description=jd['description'],
        activity_id = jd['activity_id'],
        activity_to_id = jd['activity_to_id']
        )
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        objeto = list(Contingency.objects.filter(id=id).values())
        if len(objeto) >0:
            eq=Contingency.objects.get(id=id)
            eq.idevent_id=jd['idevent_id']
            eq.description = jd['description']
            eq.activity_id=jd['activity_id']
            eq.activity_to_id=jd['activity_to_id']
            
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Personal no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        stand = list(Contingency.objects.filter(id=id).values())
        if len(stand) >0:
            Contingency.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Personal no found'}
        return JsonResponse(datos)

def contingency(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='contingency.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login




def login(request):
    if request.method == "POST":   #si le dieron al boton de accesar
        us = request.POST["usuario"]
        pas = request.POST["pass"]
        print(us)
        print('la clave es', pas)
        user = authenticate(username=us, password=pas)
        print(user)
        if user is not None:
            # Hacemos el login manualmente
            do_login(request, user)
            # Y le redireccionamos a la portada
            return redirect('/')
        else:
            return render(request,"errorpsw.html")

    return render(request, "login.html")




def usuario_activo(request):
    usuario = User.objects.get(id=request.user.id)
    return usuario


def logoutnea(request):
    # Finalizamos la sesión
    try:
        usuario = User.objects.get(id=request.user.id)
        do_logout(request)
    except:
        pass
    # Redireccionamos a la portada
    return render(request, "login.html")




class QuestionaryView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get(self, request, id=0):
        if id >0:
            objetos = list(Questionary.objects.filter(id=id).values())
            if len(objetos)>0:
                objet = objetos[0]
                datos={'message':'Success...', 'Questionary': objet}
            else:
                datos={'message': 'No found......'}
            return JsonResponse(datos)    
        else:    
            objetos = list(Questionary.objects.values())
            if len(objetos) > 0:
                datos = {'message':'Success', 'Questionary': objetos}
                
            else:
                datos = {'message': 'Error, no foud....'}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        Questionary.objects.create(idevent_id=jd['idevent_id'],
        question=jd['question'],
        response1=jd['response1'],
        response2=jd['response2'],
        response3=jd['response3'],
        response4=jd['response4'],
        validresponse=jd['validresponse'],
        activity_id=jd['activity_id']
        )
        datos = {'message': 'success....'}
        return JsonResponse(datos)

    def put(self, request,id):
        jd = json.loads(request.body)
        objeto = list(Questionary.objects.filter(id=id).values())
        if len(objeto) >0:
            eq=Questionary.objects.get(id=id)
            eq.idevent_id=jd['idevent_id']
            eq.question = jd['question']
            eq.response1 = jd['response1']
            eq.response2 = jd['response2']
            eq.response3 = jd['response3']
            eq.response4 = jd['response4']
            eq.validresponse = jd['validresponse']
            eq.activity_id=jd['activity_id']
            
            
            eq.save()
            datos={'message': 'OK-Success'}
        else:
            datos={'message':'Questionary no found'}
        return JsonResponse(datos)


    def delete(self, request, id):
        stand = list(Questionary.objects.filter(id=id).values())
        if len(stand) >0:
            Questionary.objects.filter(id=id).delete()
            datos={'message': 'Success'}
        else:
             datos={'message':'Questionary no found'}
        return JsonResponse(datos)
    

def questionary(request):
    try:
        usuario =usuario_activo(request)
        contexto={"serverhost": servidor}
        template='questionary.html'
        return render(request, template, contexto)
    except Exception as ex:
        return redirect('/login/')  # redircciona al url de  login


def qrquestion(request, pk):
    '''
    quest = Questionary.objects.filter(id=id)
    data = 'http://192.168.31.13:8000/quest/'
    ima = qrcode.make(data)

    contexto ={'quest':quest, 'qr': ima}

    template = 'qrquestion.html'
    return render(request, template, contexto)
    
    data = 'http://192.168.31.13:8000/quest/'+ str(id)
    img = qrcode.make(data)

    buf = BytesIO()		# BytesIO se da cuenta de leer y escribir bytes en la memoria
    img.save(buf)
    image_stream = buf.getvalue()

    response = HttpResponse(image_stream, content_type="image/png")
    return response
    '''
    # Otra forma
    data =   servidor+  '/quest/'+ str(pk)
    context = {} 
    factory = qrcode.image.svg.SvgImage 
    img = qrcode.make(data, image_factory=factory, box_size=50) 
    stream = BytesIO() 
    img.save(stream) 
    context["svg"] = stream.getvalue().decode() 
    context["data"] = str(pk)
    try:
        usuario =usuario_activo(request)
        user_log = usuario.first_name
    except:
        user_log="N"
    context["userlog"] = user_log    
    return render(request, "qrquestion.html", context=context)




def question(request, pk):
     quest = Questionary.objects.filter(id=pk)
     contexto = {'quest': quest}
     template = 'question.html'
     return render(request, template, contexto)


def index(request): 
    context = {} 
    if request.method == "POST": 
        factory = qrcode.image.svg.SvgImage 
        img = qrcode.make(data, image_factory=factory, box_size=20) 
        stream = BytesIO() 
        img.save(stream) 
        context["svg"] = stream.getvalue().decode() 
        return render(request, "index.html", context=context)

def saveresponse(request):
    #print ('PASA POR AQUI  LA VARIABLE ES: ')
    #return  HttpResponse(config('SERVERPORT'))
    mensajeresp = "..."
    if request.method == "POST": 
        mensajeresp = "<b>¡INCORRECTO!</b> <br> Pon mas atención para la próxima"
        resp_anterior = ResponseQuest.objects.filter(question_id=request.POST['idquest']).filter(cod_pulsera=request.POST['codigo'])
        if (resp_anterior.count() > 0):
            mensajeresp="! NO PUEDES RESPONDER VARIAS VECES LA MISMA PREGUNTA ! <br> <br> <b>¡ SIN TRAMPAS ! </b> "
        else:
            pregunta = Questionary.objects.get(id=request.POST['idquest'])
            if(int(request.POST['opt1'])==pregunta.validresponse):
                ganador=True
                mensajeresp="¡¡¡CORRECTO!!! <br> <b>FELICITACIONES</b>"
            else:
                ganador=False
            print(pregunta.validresponse)    
            print(ganador)
            respuesta = ResponseQuest.objects.create(
                idevent_id = 1,
                cod_pulsera= request.POST['codigo'],
                question_id = request.POST['idquest'],
                response = request.POST['opt1'],
                winner = ganador
            )
            respuesta.save()
    contexto = {'mensaje': mensajeresp}        
    return  render(request, 'response.html', contexto)    #HttpResponse(mensajeresp)


def Winner(request, pk):
    quest = Questionary.objects.get(id=pk)
    cuantos = 0
    try:
        responses = ResponseQuest.objects.filter(question_id=pk)
        win = random.choice(responses)
        cuantos = ResponseQuest.objects.filter(question_id=pk).count()
    except: 
        responses =""
        win = ''
        
    contexto = {'indice': pk, 'responses': responses, 'quest':quest, 'cuantos': cuantos, 'win': win}
    return render(request, 'winner.html', contexto)

