import base64
from email.policy import default
from operator import truediv
from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth.models import User
from django.utils.html import mark_safe

# Create your models here.

class IDEvent(models.Model):
    name = models.CharField(max_length=250)
    description = models.CharField(max_length=500, null=True, blank=True)
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)

    def __str__(self):
        return self.name


    class Meta:
        db_table = 'idevent'


class Participants(models.Model):
    typelist = (
        ('E', 'Expositor'),
        ('S', 'Sponsor'),
        )
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    description_expositor =  models.CharField(max_length=500, null=True, blank=True, default='')
    representant = models.CharField(max_length=250, null=True, blank=True, default='')
    dni = models.CharField(max_length=15, null=True, blank=True)
    phone = models.CharField(max_length=25, null=True, blank=True, default='')
    ubication = models.CharField(max_length=500, null=True, blank=True, default='')
    type_participant = models.CharField(max_length=5, choices=typelist, default='E')
    photo = models.CharField(max_length=200, blank=True, null=True, default='')
    

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'participants'

   


class Know_Area(models.Model):
    name_area = models.CharField(max_length=50)
    description_area = models.CharField(max_length=250)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'know_area'


class Exposition(models.Model):
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    participants = models.ForeignKey(Participants, null=True,  on_delete=models.SET_NULL)
    name = models.CharField(max_length=500)
    thema =  models.CharField(max_length=100)
    detail_exposition =  models.CharField(max_length=500)
    estimate_time_min = models.IntegerField()

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'exposition'


class Stands(models.Model):
    typestand = (
        ('G', 'GIGA'),
        ('T', 'TERA'),
        ('P', 'PETA'),
    )

    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    cod = models.CharField(max_length=15, null=True, blank=True, default='' )
    participants = models.ForeignKey(Participants, null=True,  on_delete=models.SET_NULL)
    name = models.CharField(max_length=500)
    tip=models.CharField( max_length=1, choices=typestand, default='G')

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'stands'



class Personal(models.Model):
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=500)
    phone = models.CharField(max_length=25)
    type_resp = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'personal'



class Activity(models.Model):
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=500)
    exact_date = models.DateTimeField()
    duration = models.IntegerField()
    status = models.BooleanField(default=False)
    personal = models.ForeignKey(Personal, null = True, on_delete=models.SET_NULL)
    speach = models.ForeignKey(Exposition, null=True, blank=True,  on_delete=models.SET_NULL, default='')
    description = models.CharField(max_length=500, null=True, blank=True, default='')
    stand = models.ForeignKey(Stands, null=True, blank=True, on_delete=models.SET_NULL, default='')
    intimeline = models.BooleanField(default=True)

    def __str__(self):
        return "%s" % self.name

    class Meta:
        db_table = 'activity'



class Contingency(models.Model):
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    activity = models.ForeignKey(Activity, null=True, blank=True, on_delete=models.CASCADE, default='', related_name='Activity_From')
    description = models.CharField(max_length=500)
    activity_to = models.ForeignKey(Activity, null=True, blank=True, on_delete=models.SET_NULL, related_name='Activity_To')

    def __str__(self):
        return "%s" % self.description

    class Meta:
        db_table = 'contingecy'



class ExecContingency(models.Model):
    idevent = models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=500)
    datecontingency = models.DateTimeField()
    report_by = models.ForeignKey(Personal, null=True, blank=True, on_delete=models.SET_NULL, default='')


    def __str__(self) -> str:
        return "%s" % self.description



class Questionary(models.Model):
    idevent=models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL )
    activity = models.ForeignKey(Activity, null=True, on_delete=models.SET_NULL)
    question=models.CharField(max_length=500)
    status=models.BooleanField(default=False)
    response1=models.CharField(max_length=500)
    response2=models.CharField(max_length=500)
    response3=models.CharField(max_length=500)
    response4=models.CharField(max_length=500)
    validresponse=models.IntegerField()



    def __str__(self) -> str:
        return "%s" % self.question



class ResponseQuest(models.Model):
    idevent=models.ForeignKey(IDEvent, null=True, on_delete=models.SET_NULL )
    cod_pulsera=models.CharField(max_length=50)
    question=models.ForeignKey(Questionary, null=True, on_delete=models.SET_NULL)
    response=models.CharField(max_length=2)
    winner = models.BooleanField(default=False)


    def __str__(self) -> str:
        return "%s" % self.cod_pulsera

    class Meta:
        db_table = 'responsequest'

    











    


    


   

