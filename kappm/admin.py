from django.contrib import admin
from .models import *

# Register your models here.


admin.site.register (IDEvent)
admin.site.register(Participants)
admin.site.register (Exposition)
admin.site.register (Stands)
admin.site.register (Activity)
admin.site.register (Personal)
admin.site.register (Contingency)
admin.site.register (Questionary)
admin.site.register (ResponseQuest)